import { Button, Grid } from "@mui/material";
import { Session } from "next-auth";
import { signOut } from "next-auth/react";
import React from "react";
import ConversationsWrapper from "./Conversations/ConversationsWrapper";
import FeedWrapper from "./Feeds/FeedWrapper";
import ModalProvider from "../../context/ModalContext";

interface ChatProps {
  session: Session;
}

const Chat: React.FC<ChatProps> = ({ session }) => {
  return (

      <Grid
        container
        spacing={0}
        style={{ height: '100vh' }}
      >
        <ModalProvider>
          <ConversationsWrapper session={session} />
          <FeedWrapper session={session} />
          {/* <Button onClick={() => signOut()} variant='contained'>Logout</Button> */}
        </ModalProvider>
      </Grid>
  );
};
export default Chat;
