import { Session } from "next-auth";
import React from 'react';
import {Box, Stack, styled, Typography} from '@mui/material';
import ConversationModal from "./Modal/Modal";

interface ConversationsProps {
      session: Session;
}

const ConversationList: React.FC<ConversationsProps> = ({session}) => {
      const [open, setOpen] = React.useState(false);
      const handleOpen = () => setOpen(true);
      const handleClose = () => setOpen(false);

      return (
            <Box sx={{ width: '100%' }}>
                 <Box 
                        py={2} 
                        px={4} 
                        sx={{
                              cursor: 'pointer',
                              borderRadius: 4,
                              backgroundColor: 'RGBA(0, 0, 0, 0.08)',
                        }}
                        onClick={handleOpen}
                  >
                        <Typography>
                              Find or start a conversation
                        </Typography>
                 </Box>
                 <ConversationModal
                        handleClose={handleClose}
                        isOpen={open}
                        session={session} 
                        conversations={[]} 
                        editingConversation={undefined} 
                        onViewConversation={() => {}}  
                        getUserParticipantObject={() => {}}                  
                  />
            </Box>
      )
}

export default ConversationList