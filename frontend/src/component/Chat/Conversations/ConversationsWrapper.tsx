import { Session } from "next-auth";
import React from 'react';
import {Stack} from '@mui/material';
import ConversationList from "./ConversationList";

interface ConversationsProps {
      session: Session;
}


const ConversationsWrapper: React.FC<ConversationsProps> = ({session}) => {
      return (
            <Stack 
                  direction="column"
                  py={3}
                  px={3}
                  sx={{
                        position: "relative",
                        base: true ? "none" : "flex", 
                        backgroundColor: 'RGBA(0, 0, 0, 0.04)',
                  }}
            >
                  <ConversationList session={session} />
            </Stack>
      )
}

export default ConversationsWrapper