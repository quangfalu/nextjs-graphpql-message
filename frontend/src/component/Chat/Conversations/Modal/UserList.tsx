
import { Avatar, Stack, Box, Typography, Button } from "@mui/material";
import React from "react";
import { SearchedUser } from "../../../../utils/types";

interface UserListProps {
      users: Array<SearchedUser>;
      participants: Array<SearchedUser>;
      addParticipant: (user: SearchedUser) => void;
}

const UserList: React.FC<UserListProps> = ({
      users, 
      participants,
      addParticipant
}) => {
      return (
            <>
                  {users.length === 0 ? (
                        <Stack 
                              alignItems="center" 
                              mt={3}
                        >
                              <Typography>No users found</Typography>
                        </Stack>
                  ) : (
                        <Stack mt={3} spacing={1}>
                             {users.map((user) => (
                                    <Stack
                                          spacing={1}
                                          px={1}
                                          key={user.id}
                                          alignItems="center"
                                          direction="row"
                                    >
                                          <Avatar {...stringAvatar(user.username)} />
                                          <div style={{
                                                display: 'flex', 
                                                alignItems: 'center', 
                                                justifyContent: 'space-between',
                                                width: '100%'
                                          }}>
                                                <Typography>{user.username}</Typography>
                                                <Button
                                                      onClick={() => addParticipant(user)}
                                                      disabled={
                                                            !!participants.find(
                                                                  (participant) => participant.id === user.id
                                                            )
                                                      }
                                                >
                                                      Select
                                                </Button>
                                          </div>
                                    </Stack>
                             ))}
                        </Stack>
                  )}
            </>
      )
}


export default UserList

function stringAvatar(name: string) {
      return {
        sx: {
          bgcolor: stringToColor(name),
        },
        children: `${name.split(' ')[0][0]}${name.split(' ')[1][0]}`,
      };
}

function stringToColor(string: string) {
      let hash = 0;
      let i;
    
      for (i = 0; i < string.length; i += 1) {
        hash = string.charCodeAt(i) + ((hash << 5) - hash);
      }
    
      let color = '#';
    
      for (i = 0; i < 3; i += 1) {
        const value = (hash >> (i * 8)) & 0xff;
        color += `00${value.toString(16)}`.slice(-2);
      }    
      return color;
}
    