import React, { useState } from 'react';
import {Modal, Typography, Button,TextField, Box, Stack} from '@mui/material';
import { SearchedUser, SearchUsersData, SearchUsersInputs } from '../../../../utils/types';
import { useLazyQuery } from '@apollo/client';
import UserOperations from "../../../../graphql/operations/user";
import UserSearchList from './UserList';
import Participants from './Participants';
import toast from 'react-hot-toast';
import { Session } from 'next-auth';
import { useRouter } from 'next/router';

interface ModalProps {
      isOpen: boolean;
      handleClose: () => void;
      session: Session;
      conversations: Array<any>;
      editingConversation: any | null;
      onViewConversation: (
            conversationId: string,
            hasSeenLatestMessage: boolean
      ) => void;
      getUserParticipantObject: (
            conversation: any
      ) => any;
}

const style = {
      position: 'absolute' as 'absolute',
      top: '30%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      width: 400,
      bgcolor: 'background.paper',
      border: '1px solid #fff',
      borderRadius: 3,
      boxShadow: 24,
      p: 4,
};

const ConversationModal: React.FC<ModalProps> = (props) => {
      const {isOpen, handleClose, session, editingConversation} = props
      const [username, setUsername] = useState("");
      const [participants, setParticipants] = useState<Array<SearchedUser>>([]);
      const [existingConversation, setExistingConversation] = useState<any | null>(null);
      const {user: { id: userId }} = session;
      const router = useRouter();

      const [
            searchUsers,
            {
              data,
              loading: searchUsersLoading,
              error: searchUsersError,
            },
          ] = useLazyQuery<SearchUsersData, SearchUsersInputs>(
            UserOperations.Queries.searchUsers
      );

      const onSearch = (event: React.FormEvent<HTMLFormElement>) => {
            event.preventDefault();            
            searchUsers({ variables: { username } });
            // handleClose()
      };

      const addParticipant = (user: SearchedUser) => {
            setParticipants((prev) => [...prev, user]);
            setUsername("");
      };

      const removeParticipant = (userId: string) => {
            setParticipants((prev) => prev.filter((u) => u.id !== userId));
      };

      const onSubmit = () => {
            if (!participants.length) return;
        
            const participantIds = participants.map((p) => p.id);
        
            // const existing = findExistingConversation(participantIds);
        
            // if (existing) {
            //   toast("Conversation already exists");
            //   setExistingConversation(existing);
            //   return;
            // }
        
            /**
             * Determine which function to call
             */
            editingConversation
              ? onUpdateConversation(editingConversation)
              : onCreateConversation();
      };

      const onCreateConversation = async () => {
            const participantIds = [userId, ...participants.map((p) => p.id)];
            try {
                  // const { data, errors } = await createConversation({
                  //   variables: {
                  //     participantIds,
                  //   },
                  // });
                  // if (!data?.createConversation || errors) {
                  //   throw new Error("Failed to create conversation");
                  // }
                  // const {
                  //   createConversation: { conversationId },
                  // } = data;
                  // router.push({ query: { conversationId } });
            
                  /**
                   * Clear state and close modal
                   * on successful creation
                   */
                  setParticipants([]);
                  setUsername("");
                  handleClose();
            } catch (error: any) {
                  console.log("createConversations error", error);
                  toast.error(error?.message);
            }
      }

      const onUpdateConversation = async (conversation: any) => {
            
      }

      return (
            <Modal
                  open={isOpen}
                  onClose={handleClose}
                  aria-labelledby="modal-modal-title"
                  aria-describedby="modal-modal-description"
            >
            <Box sx={style}>
                  <Typography id="modal-modal-title" variant="h6" component="h2">
                  </Typography>
                  <form onSubmit={onSearch} autoComplete='off'>
                        <Stack spacing={3}>
                              <TextField
                                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                          setUsername(event.target.value);  
                                    }}
                                    autoFocus
                                    id='size-small'
                                    margin="dense"
                                    size='small'
                                    label="Username"
                                    type="text"
                                    fullWidth
                                    // variant="standard"
                              />
                              <Button 
                                    disabled={searchUsersLoading}
                                    type="submit" 
                                    style={{width: '100%'}} 
                                    variant="contained" 
                                    color="primary"
                                    aria-label="search"
                              >
                                    Search
                              </Button>
                        </Stack>
                  </form>
                  {data?.searchUsers && (
                        <UserSearchList 
                              participants={participants}
                              addParticipant={addParticipant}
                              users={data?.searchUsers} 
                        />
                  )}
                  {participants.length !== 0 && (
                        <>
                              <Participants participants={participants} removeParticipant={removeParticipant} />

                              <Button
                                    style={{width: '100%'}} 
                                    variant="contained" 
                                    color="primary"
                                    aria-label="Conversation"
                                    onClick={onSubmit}
                              >
                                    {true ? "Update Conversation" : "Create Conversation"}
                              </Button>
                        </>
                  )}
            </Box>
          </Modal>
      )
}

export default ConversationModal