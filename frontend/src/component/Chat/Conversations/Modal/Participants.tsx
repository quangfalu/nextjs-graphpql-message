import { Stack, Typography } from "@mui/material";
import React from "react";
import { IoIosCloseCircleOutline } from "react-icons/io";
import { SearchedUser } from "../../../../utils/types";

interface ParticipantsProps {
  participants: Array<SearchedUser>;
  removeParticipant: (userId: string) => void;
}

const Participants: React.FC<ParticipantsProps> = ({
  participants,
  removeParticipant,
}) => {
  return (
      <Stack direction="row" mt={3} flexWrap="wrap" gap="1px">
            {participants.map((participant) => (
                  <Stack
                        key={participant.id}
                        direction="row"
                        alignItems="center"
                        borderRadius={4}
                        p={1}
                  >
                        <Typography>{participant.username}</Typography>
                        <IoIosCloseCircleOutline
                              size={20}
                              cursor="pointer"
                              onClick={() => removeParticipant(participant.id)}
                        />
                  </Stack>
            ))}
      </Stack>
  );
};
export default Participants;
