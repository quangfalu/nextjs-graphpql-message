import {Session} from "next-auth"
import {signIn, signOut} from "next-auth/react"
import {Button, Grid, Stack, TextField, Typography} from '@mui/material'
import {Google, Bookmark} from '@mui/icons-material';
import { useState } from "react";
import toast from "react-hot-toast";
import { useMutation } from "@apollo/client";
import UserOperations from "../../graphql/operations/user";
import { CreateUsernameData, CreateUsernameVariables } from "../../utils/types";

interface IAuthProps {
      session: Session | null;
      reloadSession: () => void
}
const Auth: React.FunctionComponent<IAuthProps> = (props) => {
      const {session, reloadSession} = props;
      const [username, setUsername] = useState("");

      const [createUsername, {data, loading, error}] = useMutation<
            CreateUsernameData,
            CreateUsernameVariables
      >(UserOperations.Mutations.createUsername) 
      
      const onSubmit = async () => {
            if (!username) return;

            try {
                  const { data } = await createUsername({variables: {username} })

                  if (!data?.createUsername) {
                       throw new Error()
                  }

                  if (data.createUsername.error) {
                        const {
                              createUsername: {error}
                        } = data;
                        throw new Error(error)
                  }

                  toast.success("Username successfully created");
                  reloadSession()
            } catch (error) {
                  toast.error("There was an error");
            }
            
      }
      return (
            <Grid
                  container
                  spacing={0}
                  direction="column"
                  alignItems="center"
                  justifyContent="center"
                  style={{ minHeight: '100vh' }}
            >
                  <Stack spacing={2} alignItems="center">
                        {session ? (
                              <>
                                    <Typography variant="h5" component="h5">
                                          Create a Username
                                    </Typography>
                                    <TextField 
                                          id='form-props-search' 
                                          label='Enter Username' 
                                          type='input' 
                                          value={username}
                                          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                                                setUsername(event.target.value)
                                          }
                                    />
                                    <Button 
                                          startIcon={<Bookmark />} 
                                          onClick={onSubmit} 
                                          variant='contained'>
                                          Save
                                    </Button>
                              </>
                        ) : (
                              <>
                                    <Typography variant="h5" component="h5">
                                          MessengerQL
                                    </Typography>
                                    <Button 
                                          startIcon={<Google />} 
                                          onClick={() => signIn('google')} 
                                          variant='contained'>
                                          Continue with Google
                                    </Button>
                              </>
                        )}
                  </Stack>
            </Grid>
      )
}

export default Auth