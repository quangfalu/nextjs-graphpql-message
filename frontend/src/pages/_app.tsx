import { SessionProvider } from 'next-auth/react'
import '../../styles/globals.css'
import type { AppProps } from 'next/app'
import { CacheProvider, EmotionCache } from '@emotion/react';
import { ThemeProvider, CssBaseline, createTheme } from '@mui/material';
import { ApolloProvider } from "@apollo/client";
import { client } from "../graphql/apollo-client";
import { Toaster } from "react-hot-toast";

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import lightThemeOptions from "../utils/theme";
import createEmotionCache from "../utils/createEmotionCache";

interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache;
}
const clientSideEmotionCache = createEmotionCache();
const lightTheme = createTheme(lightThemeOptions);

const MyApp: React.FunctionComponent<MyAppProps> = (props) => {
  const { 
    Component, 
    emotionCache = clientSideEmotionCache, 
    pageProps: {session, ...pageProps}
  } = props;

  return (
    <ApolloProvider client={client}>
      <SessionProvider session={session}>
        <CacheProvider value={emotionCache}>
          <ThemeProvider theme={lightTheme}>
            <CssBaseline />
            <Component {...pageProps} />
            <Toaster />
          </ThemeProvider>
        </CacheProvider>
      </SessionProvider>
    </ApolloProvider>
  );
};

export default MyApp;
