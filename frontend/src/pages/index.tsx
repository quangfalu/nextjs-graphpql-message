import { getSession, useSession } from "next-auth/react"
import { NextPage } from 'next';
import Auth from '../component/Auth/Auth';
import Chat from '../component/Chat';

const Home: NextPage = () => {
const { data: session } = useSession();
console.log(session, 'session');

const reloadSession = () => {
  const event = new Event("visibilitychange");
  document.dispatchEvent(event);
};

return (
    <div>
      {session && session?.user?.username ? (
        <Chat session={session} />
      ) : (
        <Auth session={session} reloadSession={reloadSession} />
      )}
    </div>
  )
}

export default Home;

