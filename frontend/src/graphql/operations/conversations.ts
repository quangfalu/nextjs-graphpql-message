import { gql } from "@apollo/client";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
      Queries: {
            // conversations: gql`
            //   query Conversations {
            //     conversations {
            //       ${ConversationFields}
            //     }
            //   }
            // `,
      },
      Mutations: {
            createConversation: gql`
                  mutation CreateConversation($participantIds: String!) {
                        createConversation(participantIds: $participantIds) {
                              conversationId
                        }
                  }
            `
      },
}