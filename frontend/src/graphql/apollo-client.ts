import { ApolloClient, HttpLink, InMemoryCache, split } from "@apollo/client";

const httpLink = new HttpLink({
      uri: 'http://localhost:4000/graphql',
      credentials: "include",
})

export const client = new ApolloClient({
      link: httpLink,
      cache: new InMemoryCache
})