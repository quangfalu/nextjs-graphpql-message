import { ApolloServer } from '@apollo/server';
import { expressMiddleware } from '@apollo/server/express4';
import { ApolloServerPluginDrainHttpServer } from '@apollo/server/plugin/drainHttpServer';
import { makeExecutableSchema } from "@graphql-tools/schema";
import { PrismaClient } from '@prisma/client';
import { getSession } from "next-auth/react";
import { json } from 'body-parser';
import express from 'express';
import * as dotenv from "dotenv";
import http from 'http';
import cors from 'cors';

import resolvers from './graphql/resolvers';
import typeDefs from "./graphql/typeDefs";
import { GraphQLContext, Session } from './utils/type';

const main = async () => {
      dotenv.config();
      const schema = makeExecutableSchema({
            typeDefs,
            resolvers,
      });
      const app = express();
      const httpServer = http.createServer(app);

      const corsOptions = {
            origin: process.env.CLIENT_ORIGIN,
            credentials: true,
      };

      const prisma = new PrismaClient()

      const server = new ApolloServer<any>({
            schema,
            csrfPrevention: true,
            plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
      });
      await server.start();

      app.use(
            '/graphql',
            cors<cors.CorsRequest>(corsOptions),
            json(),
            expressMiddleware(server, {
                  context: async ({ req, res }): Promise<GraphQLContext | null> => {
                        const session = await getSession({ req }) as Session;
                        res.header("Access-Control-Allow-Origin", process.env.CLIENT_ORIGIN);

                        return { session, prisma };
                  },
            }),

      );

      await new Promise<void>((resolve) => httpServer.listen({ port: 4000 }, resolve));
      console.log(`🚀 Server ready at http://localhost:4000/graphql`);
}

main().catch((error) => console.log(error, 'error'))