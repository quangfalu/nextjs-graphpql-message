import { PrismaClient } from "@prisma/client";
import { PubSub } from "graphql-subscriptions";
import { Context } from "graphql-ws/lib/server";
import { ISODateString } from "next-auth";

/**
 * Server Configuration
 */
export interface Session {
      user?: User;
      expires?: ISODateString;
}

export interface GraphQLContext {
      session: Session | null;
      prisma: PrismaClient;
      // pubsub: PubSub;
}

export interface SubscriptionContext extends Context {
      connectionParams: {
            session?: Session;
      };
}

/**
 * Users
 */
export interface User {
      id: string
      name: string | null
      email: string | null
      emailVerified: Date | null
      image: string | null
      username: string | null
      createdAt: Date
      updatedAt: Date
}

export interface CreateUsernameResponse {
      success?: boolean;
      error?: string;
}

export interface SearchUsersResponse {
      users: Array<User>;
}

/**
 * Messages
 */
export interface SendMessageArguments {
      id: string;
      conversationId: string;
      senderId: string;
      body: string;
}

export interface SendMessageSubscriptionPayload {
      messageSent: any;
}

/**
 * Conversations
 */



export interface ConversationCreatedSubscriptionPayload {
      conversationCreated: any;
}

export interface ConversationUpdatedSubscriptionData {
      conversationUpdated: {
            addedUserIds: Array<string>;
            removedUserIds: Array<string>;
      };
}

export interface ConversationDeletedSubscriptionPayload {
      conversationDeleted: any;
}
