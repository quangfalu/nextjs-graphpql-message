import { verifyAndCreateUsername } from "../../utils/function";
import { CreateUsernameResponse, GraphQLContext, User } from "../../utils/type";
import { GraphQLError } from 'graphql';

const resolvers = {
      Query: {
            searchUsers: async (
                  _: any,
                  args: { username: string },
                  context: GraphQLContext
            ): Promise<Array<User>> => {
                  const { session, prisma } = context
                  const { username: searchedUsername } = args

                  if (!session?.user) {
                        throw new GraphQLError("Not authorized")
                  }

                  const {
                        user: { username: myUsername },
                  } = session;

                  try {
                        const users = await prisma.user.findMany({
                              where: {
                                    username: {
                                          contains: searchedUsername,
                                          not: myUsername,
                                          mode: "insensitive",
                                    },
                              },
                        });

                        return users;
                  } catch (error: any) {
                        throw new GraphQLError(error?.message);
                  }
            }
      },
      Mutation: {
            createUsername: async (
                  _: any,
                  args: { username: string },
                  context: GraphQLContext
            ): Promise<CreateUsernameResponse> => {
                  const { session, prisma } = context

                  if (!session?.user) {
                        return {
                              error: "Not authorized"
                        }
                  }

                  const { id } = session?.user;
                  const { username } = args;

                  return await verifyAndCreateUsername({ userId: id, username }, prisma);
            }
      },
      // Subscription: {},
}

export default resolvers